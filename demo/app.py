import psycopg2
from flask import Flask, jsonify, render_template, request, redirect, session, url_for
from flask_restful import reqparse
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import update, cast, String, desc, text, Date, asc
from sqlalchemy.sql import func
import datetime
from create_script import *
import os
import subprocess
import shutil
from apscheduler.schedulers.background import BackgroundScheduler
import glob
from flask_mail import Mail, Message
import re

app = Flask(__name__)
scheduler = BackgroundScheduler()
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:1234@localhost/postgres'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = "secret_key"

app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = 'pham.nguyen.198.198@gmail.com'
app.config['MAIL_PASSWORD'] = 'rrexewjhooifeinh'
mail = Mail(app)

db = SQLAlchemy(app)

class ItemDesign(db.Model):
    __tablename__ = 't_item_design'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    type = db.Column(db.String(100))
    condition = db.Column(db.String(200))
    message = db.Column(db.String(200))
    sequence = db.Column(db.Integer)
    basicDesignId = db.Column(db.Integer)
    isDelete = db.Column(db.String(2))
    operation = db.Column(db.String(100))
    commentstatus = db.Column(db.String(2))

    def to_dict(self):
        return{
            'id':self.id,
            'name': self.name,
            'type':self.type,
            'condition':self.condition,
            'message':self.message,
            'sequence':self.sequence,
            'basicDesignId':self.basicDesignId,
            'isDelete':self.isDelete,
            'operation':self.operation,
            'commentStatus':self.commentstatus,
        }

class ItemDesignFlow(db.Model):
    __tablename__ = 't_item_design_flow'
    id = db.Column(db.Integer, primary_key=True)
    idFlow = db.Column(db.Integer)
    name = db.Column(db.String(100))
    type = db.Column(db.String(100))
    condition = db.Column(db.String(200))
    message = db.Column(db.String(200))
    sequence = db.Column(db.Integer)
    basicDesignId = db.Column(db.Integer)
    isDelete = db.Column(db.String(2))
    operation = db.Column(db.String(100))
    commentstatus = db.Column(db.String(2))
    flowName = db.Column(db.String(200))
    result = db.Column(db.String(200))

    def to_dict(self):
        return{
            'id':self.id,
            'idFlow':self.idFlow,
            'name': self.name,
            'type':self.type,
            'condition':self.condition,
            'message':self.message,
            'sequence':self.sequence,
            'basicDesignId':self.basicDesignId,
            'isDelete':self.isDelete,
            'operation':self.operation,
            'commentStatus':self.commentstatus,
        }

class ItemDesignHistory(db.Model):
    __tablename__ = 't_item_history'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    type = db.Column(db.String(100))
    condition = db.Column(db.String(200))
    message = db.Column(db.String(200))
    sequence = db.Column(db.Integer)
    basicDesignId = db.Column(db.Integer)
    isDelete = db.Column(db.String(2))
    operation = db.Column(db.String(100))
    version = db.Column(db.Integer)

    def to_dict(self):
        return{
            'id':self.id,
            'name': self.name,
            'type':self.type,
            'condition':self.condition,
            'message':self.message,
            'sequence':self.sequence,
            'basicDesignId':self.basicDesignId,
            'isDelete':self.isDelete,
            'operation':self.operation,
            'version':self.version
        }

class BasicDesign(db.Model):
    __tablename__ = 't_basic_design'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    status = db.Column(db.String(100))
    version = db.Column(db.Integer)
    updateDate = db.Column(db.Date)
    createDate = db.Column(db.Date)
    updateUser = db.Column(db.String(100))
    createUser = db.Column(db.String(100))
    isDelete = db.Column(db.String(2))

class CommentItem(db.Model):
    __tablename__ = 't_item_comment'
    id = db.Column(db.Integer, primary_key=True)
    comment = db.Column(db.String(300))
    itemId = db.Column(db.Integer)
    createUser = db.Column(db.String(100))
    createDate = db.Column(db.DateTime)
    isDelete = db.Column(db.String(2))

class User(db.Model):
    __tablename__ = 't_user'
    id = db.Column(db.Integer, primary_key=True)
    userName = db.Column(db.String(100))
    name = db.Column(db.String(200))
    role = db.Column(db.String(2))
    createDate = db.Column(db.DateTime)
    isDelete = db.Column(db.String(2))
    password = db.Column(db.String(200))


class Feature(db.Model):
    __tablename__ = 't_feature'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    createUser = db.Column(db.String(100))
    createDate = db.Column(db.DateTime)
    updateUser = db.Column(db.String(100))
    updateDate = db.Column(db.DateTime)
    isDelete = db.Column(db.String(2))
    version = db.Column(db.Integer)

class Scenario(db.Model):
    __tablename__ = 't_scenario'
    id = db.Column(db.Integer, primary_key=True)
    scenario = db.Column(db.String(200))
    background = db.Column(db.String(400))
    given = db.Column(db.String(400))
    when = db.Column(db.String(400))
    then = db.Column(db.String(400))
    sequence = db.Column(db.Integer)
    but = db.Column(db.String(400))
    examples = db.Column(db.String(400))
    scenarioOutline = db.Column(db.String(400))
    idFeature = db.Column(db.Integer)
    isFlow = db.Column(db.String(2))

class ScriptStepDetail(db.Model):
    __tablename__ = 't_script_step_detail'
    id = db.Column(db.Integer, primary_key=True)
    idScenario = db.Column(db.Integer)
    idScript = db.Column(db.Integer)
    scenario = db.Column(db.String(200))
    nameFile = db.Column(db.String(200))
    content = db.Column(db.String(10000))
    isFlow = db.Column(db.String(2))

class ScriptStep(db.Model):
    __tablename__ = 't_script_step'
    id = db.Column(db.Integer, primary_key=True)
    idFeature = db.Column(db.Integer)
    name = db.Column(db.String(200))
    scheduler = db.Column(db.String(5))
    createUser = db.Column(db.String(100))
    createDate = db.Column(db.DateTime)

class ReportScript(db.Model):
    __tablename__ = 't_report_script'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    totalCase = db.Column(db.Integer)
    passCase = db.Column(db.Integer)
    failCase = db.Column(db.Integer)
    idScript = db.Column(db.Integer)
    updateDate = db.Column(db.Date)
    createDate = db.Column(db.Date)
    updateUser = db.Column(db.String(100))
    createUser = db.Column(db.String(100))
    isDelete = db.Column(db.String(2))

class ReportScriptDetail(db.Model):
    __tablename__ = 't_report_script_detail'
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(1000))
    idReportScript = db.Column(db.Integer)
    idScriptDetail = db.Column(db.Integer)
    idScenario = db.Column(db.Integer)
    createDate = db.Column(db.Date)
    createUser = db.Column(db.String(100))
    isDelete = db.Column(db.String(2))
    runStatus = db.Column(db.String(2))

@app.route('/create-items-basic-design', methods=["POST","GET"])
def create_items_design():
    if request.method == "POST":
        parser = reqparse.RequestParser()
        parser.add_argument('data', type=list, location='json')
        args = parser.parse_args()
        myData = args['data']
        basicDesign = ''
        basicDesignIdOld = myData[0]['basicDesignId']
        
        if basicDesignIdOld == '':
            basicDesignId = create_basic_design(myData[0]['name'], myData[0]['status'], myData[0]['createUser']);
        else:
            if myData[0]['status'] != 'Creating':
                rows = BasicDesign.query.filter_by(id=basicDesignIdOld).first()
                update_basic_design(myData[0]['status'], basicDesignIdOld, '', myData[0]['createUser'])
            basicDesignId = basicDesignIdOld
            delete_items_design(basicDesignIdOld);
        
        for i, data in enumerate(myData[1]):
            itemDesign = ItemDesign(id=None, name=data['name'], type=data['type'], condition=data['condition'], message=data['message'], sequence=data['sequence'], basicDesignId=basicDesignId, isDelete='0', operation=data['operation'], commentstatus = '0')
            db.session.add(itemDesign)

        for i, data in enumerate(myData[2]):
            itemDesignFlow = ItemDesignFlow(id=None, idFlow=data['sequence'], name=data['name'], type=data['type'], condition=data['condition'], sequence=data['sequence'], basicDesignId=basicDesignId, isDelete='0', operation=data['operation'], commentstatus = '0', flowName=data['flowName'], result=data['result'])
            db.session.add(itemDesignFlow)
        
        db.session.commit()
        return jsonify('Item design added successfully'), 200
    id = request.args.get('id')
    rows = []
    if id != '':
        id = request.args.get('id')
        rows = ItemDesign.query.filter_by(basicDesignId=id).all()
        basicDesign = BasicDesign.query.filter_by(id=id).all()
    return render_template('basic_design/create.html', listdata=rows, basicDesign=basicDesign)

@app.route('/review-items-basic-design', methods=["POST","GET"])
def review_items_design():
    id = request.args.get('id')
    subQuery = db.session.query(func.string_agg(CommentItem.createUser + '$$$' + cast(CommentItem.createDate, String) + '$$$' + CommentItem.comment, '|||')).filter(CommentItem.itemId == ItemDesign.id).correlate(ItemDesign).as_scalar()
    rows = db.session.query(ItemDesign, subQuery.label('commentUser')).filter(ItemDesign.basicDesignId ==id).order_by(ItemDesign.sequence).all()
    itemDesignFlow = db.session.query(ItemDesignFlow, subQuery.label('commentUser')).filter(ItemDesignFlow.basicDesignId ==id).order_by(ItemDesignFlow.sequence).all()
    basicDesign = BasicDesign.query.filter_by(id=id).all()
    return render_template('basic_design/review.html', listdata=rows, basicDesign=basicDesign, itemDesignFlow=itemDesignFlow)

@app.route('/view-items-basic-design', methods=["POST","GET"])
def view_items_design():
    id = ''
    if request.method == "POST":
        data = request.get_json()
        id = data['data']['id']
        version = data['data']['version']
        if version == 'latest':
            rows = db.session.query(ItemDesign).filter(ItemDesign.basicDesignId ==id).order_by(ItemDesign.sequence).all()
        else:
            rows = db.session.query(ItemDesignHistory).filter(ItemDesignHistory.basicDesignId == id, ItemDesignHistory.version == version).order_by(ItemDesignHistory.sequence).all()
        return jsonify([data.to_dict() for data in rows])
    else:
        id = request.args.get('id')
        rows = db.session.query(ItemDesign).filter(ItemDesign.basicDesignId ==id).order_by(ItemDesign.sequence).all()
    basicDesign = BasicDesign.query.filter_by(id=id).all()
    return render_template('basic_design/view.html', listdata=rows, basicDesign=basicDesign, version= range(0,basicDesign[0].version + 1))

@app.route('/edit-items-basic-design', methods=["POST","GET"])
def edit_items_design():
    if request.method == "POST":
        parser = reqparse.RequestParser()
        parser.add_argument('data', type=list, location='json')
        args = parser.parse_args()
        myData = args['data']
        update_basic_design(myData[2]['status'], myData[2]['basicDesignId'],'', myData[2]['createUser'])
        update_items_design(myData[1],'')
        create_comment_item(myData[0])
        return jsonify('Item design update successfully'), 200

    id = request.args.get('id')
    subQuery = db.session.query(func.string_agg(CommentItem.createUser + '$$$' + cast(CommentItem.createDate, String) + '$$$' + CommentItem.comment, '|||')).filter(CommentItem.itemId == ItemDesign.id).correlate(ItemDesign).as_scalar()
    rows = db.session.query(ItemDesign, subQuery.label('commentUser')).filter(ItemDesign.basicDesignId ==id).order_by(ItemDesign.sequence).all()
    basicDesign = BasicDesign.query.filter_by(id=id).all()
    return render_template('basic_design/edit.html', listdata=rows, basicDesign=basicDesign)

@app.route('/add-comment', methods=["POST"])
def add_comment_item():
    parser = reqparse.RequestParser()
    parser.add_argument('data', type=list, location='json')
    args = parser.parse_args()
    myData = args['data']
    update_basic_design(myData[1]['status'],myData[1]['basicDesignId'],'comment', myData[1]['createUser'])
    update_items_design(myData[2],'commentStatus');
    if len(myData[0]) < 1:
        return jsonify('Change status successfully'), 200
    
    create_comment_item(myData[0])
    return jsonify('Comment added successfully'), 200

def create_items_design_history(id):
    basicDesign = BasicDesign.query.filter_by(id=id).first()
    rows = ItemDesign.query.filter_by(basicDesignId=id).all()
    for data in rows:
        itemDesignHistory = ItemDesignHistory(id=data.id, name=data.name, type=data.type, condition=data.condition, message=data.message, sequence=data.sequence, basicDesignId=id, isDelete=data.isDelete, operation=data.operation, version=basicDesign.version)
        db.session.add(itemDesignHistory)
    db.session.commit()

def create_comment_item(myData):
    for data in myData:
        date = datetime.datetime.strptime(data['createDate'], '%Y-%m-%d %H:%M:%S')
        commentItem = CommentItem(id=None, comment=data['comment'], itemId=data['itemId'], createDate = date, createUser=data['createUser'], isDelete='0')
        db.session.add(commentItem)
    db.session.commit()

def update_items_design(myData,status):
    stmt = ''
    if status == 'commentStatus':
        stmt = text("UPDATE t_item_design SET commentstatus = :commentstatus WHERE id = :id")
        for data in myData:
            db.session.execute(stmt.bindparams(commentstatus=data['commentStatus'], id=data['id']))
    else:
        stmt = text("UPDATE t_item_design SET name = :name, type = :type, operation= :operation, condition = :condition, message = :message  WHERE id = :id")
        for data in myData:
            db.session.execute(stmt.bindparams(name=data['name'], type=data['type'], operation=data['operation'], condition=data['condition'], message=data['message'], id=data['id']))
    db.session.commit()

def create_basic_design(nameScreen, status, createUser):
    basicDesign = BasicDesign(id=None, name=nameScreen, status=status, version=0, createDate=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), createUser=createUser)
    db.session.add(basicDesign)
    db.session.flush()
    db.session.commit()
    return basicDesign.id

def update_basic_design(status, id, function, createUser):
    rows = BasicDesign.query.filter_by(id=id).first()
    listStatus = ['Completed creation and is requesting review','Reviewing','Done','Completed fix and is requesting review']
    if status != 'Fixing' and status != 'Reviewing' and status != 'Completed review and is requesting fix' and status != 'Done':
        if rows.status not in listStatus:
            create_items_design_history(id)
            rows.version = rows.version + 1
    rows.status = status
    rows.updateUser = createUser
    rows.updateDate = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    db.session.commit()

@app.route('/delete-basic-design', methods=["GET"])
def delete_basic_design():
    id = request.args.get('id')
    db.session.query(BasicDesign).filter(BasicDesign.id == id).delete()
    delete_items_design(id)
    db.session.commit()
    return jsonify('Design delete successfully'), 200

def delete_items_design(id):
    db.session.query(ItemDesign).filter(ItemDesign.basicDesignId == id).delete()

@app.route('/logout', methods=["POST","GET"])
def logout():
    session.clear();
    return "logout", 200

@app.route('/menu', methods=["GET"])
def menu():
    return render_template('menu.html')

@app.route('/login', methods=["POST","GET"])
def login():
    if request.method == "POST":
        userName = request.form['username']
        passWord = request.form['password']
        user = db.session.query(User).filter(User.userName == userName, User.password == passWord).first()
        if user:
            session['userName'] = userName
            return redirect('menu')
        else:
            return jsonify('The email or password is incorrect.'), 200
    return render_template('login.html')

@app.route('/list-design', methods=["POST","GET"])
def get_list_design():
    # conn = psycopg2.connect(dbname="postgres", host="localhost", user="postgres", password="1234", port="5432")
    # cursor = conn.cursor()
    # cursor.execute('SELECT * FROM t_basic_design')
    # rows = cursor.fetchall()
    if 'userName' not in session:
        return redirect(url_for('login'))
    status = ''
    nameScreen= ''
    createDate=''
    nameScreenNew=''
    if request.method == "POST":
        status = request.form['status']
        nameScreen = request.form['name_screen']
        createDate = request.form['create_date']
        if createDate:
            createDate = datetime.datetime.strptime(createDate, '%Y/%m/%d').date()
        if nameScreen:
            nameScreenNew = "%{}%".format(nameScreen)
            rows = db.session.query(BasicDesign).filter(BasicDesign.status == status if status else True, BasicDesign.name.like(nameScreenNew if nameScreenNew else True), cast(BasicDesign.createDate, Date) == createDate if createDate else True).order_by(desc(BasicDesign.createDate)).all()
        else:
            rows = db.session.query(BasicDesign).filter(BasicDesign.status == status if status else True, cast(BasicDesign.createDate, Date) == createDate if createDate else True).order_by(desc(BasicDesign.createDate)).all()
        
        if createDate:
            createDate = createDate.strftime('%Y/%m/%d')
    else:
        rows = db.session.query(BasicDesign).order_by(desc(BasicDesign.createDate)).all()

    return render_template('basic_design/list.html', listdata=rows, status=status, nameScreen=nameScreen, createDate=createDate)

@app.route('/create-scenario', methods=["POST","GET"])
def create_scenario():
    data = request.get_json()
    id = data['id']
    basicDesign = BasicDesign.query.filter_by(id=id).all()
    feature = Feature(id=None, name=basicDesign[0].name , version=0, createDate=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), createUser=session['userName'])
    db.session.add(feature)

    itemDesign = ItemDesign.query.filter_by(basicDesignId=id).all()
    for data in itemDesign:
        if data.condition != '' and data.type != 'BUTTON':
            scenario = 'Check placeholder value of item {}'.format(data.name)
            background = ''
            given = 'Open the swaglabsmobileapp app'
            when = 'Access {} screen'.format(basicDesign[0].name)
            then = 'Check {0} item placeholder value is "{1}"'.format(data.name, data.condition)
            scenario = Scenario(id=None, scenario=scenario, background=background, given=given, when=when, then=then, sequence = data.sequence, but='', examples='', scenarioOutline='', idFeature=feature.id, isFlow='0')
            db.session.add(scenario)
        elif data.type == 'BUTTON':
            if data.operation == 'TAP':
                scenario = 'Check action of button {}'.format(data.name)
                background = ''
                given = 'Open swaglabsmobileapp app'
                when = 'Access the {} screen\n'.format(basicDesign[0].name)
                when += 'Tap button "{}"'.format(data.name)
                then = 'Go to "{}" screen'.format(data.message)
                scenario = Scenario(id=None, scenario=scenario, background=background, given=given, when=when, then=then, sequence = data.sequence, but='', examples='', scenarioOutline='', idFeature=feature.id, isFlow='0')
                db.session.add(scenario)
    for data in itemDesign:
        if data.message != '' and data.type != 'BUTTON':
            scenario = 'Check display value of item {}'.format(data.name)
            background = ''
            given = 'Open swaglabsmobileapp app'
            when = 'Access {} screen'.format(basicDesign[0].name)
            then = 'Check {0} item display value is "{1}"'.format(data.name, data.message)
            scenario = Scenario(id=None, scenario=scenario, background=background, given=given, when=when, then=then, sequence = data.sequence, but='', examples='', scenarioOutline='', idFeature=feature.id, isFlow='0')
            db.session.add(scenario)
        elif data.type == 'BUTTON' and data.message != '' and data.operation == 'DISPLAY':
            scenario = 'Check display value of item {}'.format(data.name)
            background = ''
            given = 'Open swaglabsmobileapp app'
            when = 'Access {} screen'.format(basicDesign[0].name)
            then = 'Check {0} item display value is "{1}"'.format(data.name, data.message)
            scenario = Scenario(id=None, scenario=scenario, background=background, given=given, when=when, then=then, sequence = data.sequence, but='', examples='', scenarioOutline='', idFeature=feature.id, isFlow='0')
            db.session.add(scenario)
    itemDesignFlow = ItemDesignFlow.query.filter_by(basicDesignId=id).all()
    maxFlow = db.session.query(func.max(ItemDesignFlow.idFlow)).filter_by(basicDesignId=id).scalar()
    if maxFlow:
        for i in range(1,maxFlow+1):
            then = ''
            when = ''
            for data in itemDesignFlow:
                if data.idFlow == i:
                    if data.flowName != '':
                        scenario = data.flowName
                    if data.result != '':
                        then = data.result
                    if data.type == 'BUTTON':
                        when = when + 'Tap button "{}"'.format(data.name)
                    if data.type == 'TEXTBOX':
                        when = when + 'At "{0}" item enter "{1}"\n'.format(data.name, data.condition)
                    background = ''
                    given = 'Open demo app and Access the {} screen'.format(basicDesign[0].name)
            scenario = Scenario(id=None, scenario=scenario, background=background, given=given, when=when, then=then, but='', examples='', scenarioOutline='', idFeature=feature.id, isFlow='1')
            db.session.add(scenario)
    db.session.flush()
    db.session.commit()
    return jsonify('Scenario created successfully'), 200

@app.route('/list-scenario', methods=["POST","GET"])
def get_list_scenario():
    # conn = psycopg2.connect(dbname="postgres", host="localhost", user="postgres", password="1234", port="5432")
    # cursor = conn.cursor()
    # cursor.execute('SELECT * FROM t_basic_design')
    # rows = cursor.fetchall()
    if 'userName' not in session:
        return redirect(url_for('login'))
    rows = db.session.query(Feature).order_by(desc(Feature.createDate)).all()
    return render_template('scenario/list.html', listdata=rows)

@app.route('/list-script', methods=["POST","GET"])
def get_list_script():
    if 'userName' not in session:
        return redirect(url_for('login'))
    rows = db.session.query(ScriptStep).order_by(desc(ScriptStep.createDate)).all()
    return render_template('script/list.html', listdata=rows)

@app.route('/list-report', methods=["POST","GET"])
def get_list_report():
    if 'userName' not in session:
        return redirect(url_for('login'))
    rows = db.session.query(ReportScript).order_by(desc(ReportScript.createDate)).all()
    return render_template('report/list.html', listdata=rows)

@app.route('/create-script', methods=["POST","GET"])
def create_script():
    id = request.args.get('id')
    feature = db.session.query(Feature).filter(Feature.id ==id).all()
    rows = db.session.query(Scenario).filter(Scenario.idFeature ==id).all()
    result = ''
    scriptStep = ScriptStep(id=None, idFeature = feature[0].id, name = feature[0].name, createUser = session['userName'], createDate = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    db.session.add(scriptStep)
    db.session.commit()
    for item in rows:
        if not os.path.exists('features_backup/{}'.format(feature[0].name)):
            os.makedirs('features_backup/{}'.format(feature[0].name))
        f = open("features_backup/{0}/{1}.feature".format(feature[0].name, item.id), "w")
        f.write("Feature: {}\n".format(feature[0].name))
        f.write('Scenario: {}\n'.format(item.scenario))
        f.write('Given {}\n'.format(item.given))
        dataWhen = item.when.split('\n')
        i = 0
        for data in dataWhen:
            if i == 0:
                f.write('When {}\n'.format(data))
            else:
                if data != '':
                    f.write('And {}\n'.format(data))
            i += 1
        f.write('Then {}\n'.format(item.then))
        f.flush()
        f.close()
        scriptStepDetail = ''
        if item.isFlow == '0':
            result = generate_step_definitions("features_backup/{0}/{1}.feature".format(feature[0].name, item.id))
            with open("steps_backup/{0}/step_{1}.py".format(feature[0].name, item.id), 'r', encoding='cp932', errors='ignore') as f:
                feature_content = f.read()
                scriptStepDetail = ScriptStepDetail(id=None, scenario = item.scenario, idScript = scriptStep.id, content = feature_content, idScenario = item.id, isFlow = item.isFlow, nameFile = '{}_step.py'.format(item.id))
        else:
            result = generate_step_flow_definitions("features_backup/{0}/{1}.feature".format(feature[0].name, item.id))
            with open("steps_backup/{0}/step_{1}.py".format(feature[0].name, item.id), 'r', encoding='cp932', errors='ignore') as f:
                feature_content = f.read()
                scriptStepDetail = ScriptStepDetail(id=None, scenario = item.scenario, idScript = scriptStep.id, content = feature_content, idScenario = item.id, isFlow = item.isFlow, nameFile = '{}_step.py'.format(item.id))
        
        db.session.add(scriptStepDetail)
        db.session.commit()
    return jsonify(result), 200

@app.route('/delete-script', methods=["POST","GET"])
def delete_scenario():
    id = request.args.get('id')
    db.session.query(ScriptStep).filter(ScriptStep.id == id).delete()
    db.session.query(ScriptStepDetail).filter(ScriptStepDetail.idScript == id).delete()
    db.session.commit()
    return jsonify('Script delete successfully'), 200

@app.route('/delete-report', methods=["POST","GET"])
def delete_report():
    id = request.args.get('id')
    db.session.query(ReportScript).filter(ReportScript.id == id).delete()
    db.session.query(ReportScriptDetail).filter(ReportScriptDetail.idReportScript == id).delete()
    db.session.commit()
    return jsonify('Script delete successfully'), 200

@app.route('/delete-scenario', methods=["POST","GET"])
def delete_script():
    id = request.args.get('id')
    db.session.query(Feature).filter(Feature.id == id).delete()
    db.session.query(Scenario).filter(Scenario.idFeature == id).delete()
    db.session.commit()
    return jsonify('Scenario delete successfully'), 200

@app.route('/edit-scenario', methods=["POST","GET"])
def edit_scenario():
    if request.method == "POST":
        data = request.get_json()
        id = data['data']['id']
        scenario = Scenario.query.filter_by(id=id).first()
        scenario.scenario = data['data']['scenario']
        scenario.background = data['data']['background']
        scenario.given = data['data']['given']
        scenario.when = data['data']['when']
        scenario.then = data['data']['then']
        scenario.examples = data['data']['examples']
        scenario.scenarioOutline = data['data']['scenarioOutline']
        db.session.commit()
        return jsonify('Scenario update successfully'), 200
    id = request.args.get('id')
    feature = request.args.get('feature')
    rows = db.session.query(Scenario).filter(Scenario.id ==id).all()
    return render_template('scenario/edit.html', listdata=rows,feature=feature)

@app.route('/view-scenario', methods=["POST","GET"])
def view_scenario():
    id = request.args.get('id')
    feature = db.session.query(Feature).filter(Feature.id ==id).all()
    rows = db.session.query(Scenario).filter(Scenario.idFeature ==id).order_by(asc(Scenario.sequence)).all()
    return render_template('scenario/view.html', listdata=rows, feature=feature)

@app.route('/view-script', methods=["POST","GET"])
def view_script():
    id = request.args.get('id')
    scriptStep = db.session.query(ScriptStep).filter(ScriptStep.id ==id).all()
    rows = db.session.query(ScriptStepDetail).filter(ScriptStepDetail.idScript ==id).all()
    return render_template('script/view.html', listdata=rows, script=scriptStep)

@app.route('/view-report', methods=["POST","GET"])
def view_report():
    id = request.args.get('id')
    reportScript = db.session.query(ReportScript).filter(ReportScript.id ==id).all()
    rows = db.session.query(ReportScriptDetail.idScriptDetail, ReportScriptDetail.content,Scenario.scenario, Scenario.id, Scenario.isFlow).join(Scenario, ReportScriptDetail.idScenario == Scenario.id).filter(ReportScriptDetail.idReportScript ==id).all()
    return render_template('report/view.html', listdata=rows, report=reportScript)

@app.route('/run-script', methods=["POST","GET"])
def run_script():
    idScript = request.args.get('idScript')
    name = request.args.get('name')
    idScriptDetail = request.args.get('idScriptDetail')
    flg = request.args.get('flg')

    if flg == '0':
        run_script_from_scheduler(idScript, name)
        return jsonify('Script run successfully'), 200  
    # feature_files = ['features/Login/'+id+'.feature']
    # config = Configuration([feature_files])
    # runner = Runner(config)
    # runner = behave_main(args=['-k', 'features/Login/1.feature'])
    destination_file_feature = 'features_backup/{0}/{1}.feature'.format(name,idScriptDetail)
    source_file_feature = 'features/'
    shutil.copy(destination_file_feature, source_file_feature)

    destination_file_step = 'steps_backup/{0}/step_{1}.py'.format(name,idScriptDetail)
    source_file_step = 'features/steps/'
    shutil.copy(destination_file_step, source_file_step)

    command = 'behave --no-capture --no-capture-stderr --no-skipped -k -f allure_behave.formatter:AllureFormatter -o allure_result_folder_{0} -f pretty features/{1}.feature'.format(name, idScriptDetail)
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    stdout, stderr = process.communicate()
    print(stdout.decode())

    reportScript = ReportScript.query.filter_by(idScript=idScript).first()
    if reportScript == None:
        reportScript = ReportScript(id=None, name=name, totalCase = 10, passCase = 5, failCase = 5, idScript = idScript,createUser = session['userName'], createDate = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        db.session.add(reportScript)

    db.session.query(ReportScriptDetail).filter(ReportScriptDetail.idScriptDetail == idScriptDetail).delete()
    reportScriptDetail = ReportScriptDetail(id=None, idScriptDetail = idScriptDetail, idReportScript = reportScript.id,  idScenario = idScriptDetail, content = stdout.decode(),createUser = session['userName'], createDate = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    db.session.add(reportScriptDetail)
    db.session.commit()
    os.remove(source_file_step+'step_{}.py'.format(idScriptDetail))
    os.remove(source_file_feature+'{}.feature'.format(idScriptDetail))
    return jsonify('Script run successfully'), 200

@app.route('/edit-scheduler', methods=["POST","GET"])
def edit_scheduler():
    id = request.args.get('id')
    schedulerParam = request.args.get('scheduler')
    if schedulerParam != '' and schedulerParam != 'None':
        rows = ScriptStep.query.filter_by(id=id).first()
        rows.scheduler = schedulerParam
        rows.updateUser = session['userName']
        rows.updateDate = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        db.session.commit()
        send_mail()
    return jsonify('Scheduler edit successfully'), 200 

@app.after_request
def setting_scheduler(response):
    # rows = ScriptStep.query.first()
    conn = psycopg2.connect(dbname="postgres", host="localhost", user="postgres", password="1234", port="5432")
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM t_script_step')
    rows = cursor.fetchall()
    scheduler.remove_all_jobs()
    for item in rows:
        if item[5] != '' and item[5] != None and item[5] != 'None':
            hour, minute  = item[5].split(':')
            scheduler.add_job(run_script_from_scheduler, 'cron', args=(item[0],item[2]), minute=minute, hour=hour)
    return response

def run_script_from_scheduler(id,name):
    # src_files = os.listdir('features_backup/{}/'.format(name))
    # source_file_feature = 'features/'
    # for file_name in src_files:
    #     full_file_name = os.path.join('features_backup/{}/'.format(name), file_name)
    #     shutil.copy(full_file_name, source_file_feature)
    # print('tessttttttttttt')
    # src_files = os.listdir('steps_backup/{}/'.format(name))
    # source_file_step = 'features/steps/'
    # for file_name in src_files:
    #     full_file_name = os.path.join('steps_backup/{}/'.format(name), file_name)
    #     shutil.copy(full_file_name, source_file_step)
    print('tessttttttttttt')
    source_file_feature = 'features/'
    source_file_step = 'features/steps/'
    scriptStepDetail = None
    with app.app_context():
        num_passed = 0
        num_failed = 0
        arrayResult = []
        scriptStepDetail = ScriptStepDetail.query.filter_by(idScript=id).all()
        for data in scriptStepDetail:
            destination_file_feature = 'features_backup/{0}/{1}.feature'.format(name,data.idScenario)
            source_file_feature = 'features/'
            shutil.copy(destination_file_feature, source_file_feature)

            destination_file_step = 'steps_backup/{0}/step_{1}.py'.format(name,data.idScenario)
            source_file_step = 'features/steps/'
            shutil.copy(destination_file_step, source_file_step)
            command = 'behave --no-capture -f allure_behave.formatter:AllureFormatter -o allure_result_folder_{0} -f pretty features/{1}.feature'.format(name,data.idScenario)
            process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            stdout, stderr = process.communicate()
            print()
            if re.search('Failing', stdout.decode(), re.MULTILINE):
                num_failed += 1
                result = {'status':'1', 'content':stdout.decode(), 'idScenario':data.idScenario, 'id':data.id}
                arrayResult.append(result)
            else:
                num_passed +=1
                result = {'status':'0', 'content':stdout.decode(), 'idScenario':data.idScenario, 'id':data.id}
                arrayResult.append(result)

            os.remove(source_file_step+'step_{}.py'.format(data.idScenario))
            os.remove(source_file_feature+'{}.feature'.format(data.idScenario))
        reportScript = ReportScript.query.filter_by(idScript=id).first()
        if reportScript == None:
            reportScript = ReportScript(id=None, name=name, totalCase = len(scriptStepDetail), passCase = num_passed, failCase = num_failed, idScript = id,createUser = session['userName'], createDate = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            db.session.add(reportScript)
            db.session.commit()

        for data in arrayResult:
            db.session.query(ReportScriptDetail).filter(ReportScriptDetail.idScriptDetail == data['id']).delete()
            reportScriptDetail = ReportScriptDetail(id=None, idScriptDetail = data['id'], idReportScript = reportScript.id,  idScenario = data['idScenario'], content = data['content'], runStatus = data['status'],createUser = session['userName'], createDate = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            db.session.add(reportScriptDetail)
            
        db.session.commit()

    # command = 'behave --no-capture --no-capture-stderr -k features/'
    # process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    # stdout, stderr = process.communicate()
    # print(stdout.decode())

    # file_list = glob.glob(os.path.join('features/', '*.feature'))
    # for file_name in file_list:
    #      os.remove(file_name)

    # file_list = glob.glob(os.path.join('features/steps/', '*.py'))
    # for file_name in file_list:
    #      os.remove(file_name)
    return 'OK'

def send_mail():
    msg = Message('Hello', sender='pham.nguyen.198.198@gmail.com', recipients=['vannguyen09061998@gmail.com'])
    msg.body = "This is a test email sent from Flask using Flask-Mail."
    mail.send(msg)
    return "Email sent"

@app.route('/allure_report', methods=["GET"])
def allure_report():
    name = request.args.get('name')
    command = 'allure serve allure_result_folder_{}.'.format(name)
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    process.communicate()
    return "OK"

scheduler.start()
if __name__ == '__main__':
    app.run(host="localhost", port=5000)
    