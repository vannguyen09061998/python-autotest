from behave import given, when, then, step
from pathlib import Path
import re
import os

def generate_step_definitions(feature_file_path):
    with open(feature_file_path, 'r', encoding='cp932', errors='ignore') as f:
        feature_content = f.read()

    steps = []
    step_func = ''
    step_text = ''
    for line in feature_content.splitlines():
        step_content = ''
        if line.startswith('Given'):
            step_func = 'given'
            step_text = line[6:].strip()
            if step_text == 'Open swaglabsmobileapp app':
                step_content = "    desired_caps = {\n"
                step_content += '        "platformName": "Android",\n'
                step_content += '        "appium:platformVersion": "12",\n'
                step_content += '        "appium:deviceName": "Redmi Note 10 Pro",\n'
                step_content += '        "appium:appPackage": "com.swaglabsmobileapp",\n'
                step_content += '        "appium:appActivity": "com.swaglabsmobileapp.MainActivity"\n    }\n\n'
                step_content += '    context.driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_caps)\n\n'

        elif line.startswith('When'):
            step_func = 'when'
            step_text = line[5:].strip()
            step_content = '    pass\n\n'
        elif line.startswith('Then'):
            step_func = 'then'
            step_text = line[5:].strip()
            if 'placeholder' in step_text:
                item = re.compile(
                    'Check (.*) item placeholder value is "(.*)"')
                result = item.search(line)
                step_content = '    input_element = context.driver.find_element(by=MobileBy.ACCESSIBILITY_ID, value="{}")\n'.format(
                    result.group(1))
                step_content += '    placeholder_value = input_element.get_attribute("placeholder")\n'
                step_content += '    Assert.assertEquals(placeholder_value, "{}")\n\n'.format(
                    result.group(2))
            elif 'display' in step_text:
                item = re.compile('Check (.*) item display value is "(.*)"')
                result = item.search(line)
                step_content = '    display_element = context.driver.find_element(by=MobileBy.ACCESSIBILITY_ID, value="{}")\n'.format(
                    result.group(1))
                step_content += '    value = display_element.text\n'
                step_content += '    assert value == "{}"\n\n'.format(
                    result.group(2))
            # else:
            #     item = re.compile('Go to "(.*)" screen')
            #     result = item.search(line)
            #     step_content = '    assert "{}" in context.driver.title\n'.format(result.group(1))
        else:
            if step_func == 'when':
                step_text = line[3:].strip()
                steps.append(f"@{step_func}('{step_text}')\ndef step_impl(context):\n")
                if 'Tap' in line:
                    result = search('And Tap button (.*)',line.strip())
                    step_content = '    context.driver.find_element(by=MobileBy.ACCESSIBILITY_ID, value="{}").click()\n\n'.format(result.group(1))
                    steps.append(step_content)
            continue
        steps.append(f"@{step_func}('{step_text}')\ndef step_impl(context):\n")
        if step_content == '':
            steps.append('    pass\n\n')
        else:
            steps.append(step_content)

    feature_file_path = feature_file_path.split('/')
    step_file_path = 'steps_backup'+'/'+feature_file_path[1]+'/step_'+feature_file_path[2].split('.')[0]+'.py'
    path = step_file_path.split('/')
    if not os.path.exists(path[0]+'/'+path[1]):
            os.makedirs(path[0]+'/'+path[1])
    with open(step_file_path, 'w') as f:
        f.write('from behave import given, when, then, step\n')
        f.write('from appium import webdriver\n')
        f.write('from appium.webdriver.common.mobileby import MobileBy\n')
        f.write('from selenium.webdriver.support.ui import WebDriverWait\n')
        f.write('from selenium.webdriver.support import expected_conditions as EC\n\n')
        f.writelines(steps)
        f.flush()
        f.close()
    return 'Script created successfully'

def search(patten, data):
    item = re.compile(patten)
    result = item.search(data)
    return result

def generate_step_flow_definitions(feature_file_path):
    with open(feature_file_path, 'r', encoding='cp932', errors='ignore') as f:
        feature_content = f.read()
    steps = []
    step_func = ''
    step_text = ''
    for line in feature_content.splitlines():
        step_content = ''
        if line.startswith('Given'):
            step_text = line[6:].strip()
            step_func = 'given'
            step_content = "    desired_caps = {\n"
            step_content += '        "platformName": "Android",\n'
            step_content += '        "appium:platformVersion": "12",\n'
            step_content += '        "appium:deviceName": "Redmi Note 10 Pro",\n'
            step_content += '        "appium:appPackage": "com.swaglabsmobileapp",\n'
            step_content += '        "appium:appActivity": "com.swaglabsmobileapp.MainActivity"\n    }\n\n'
            step_content += '    context.driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_caps)\n\n'

        elif line.startswith('When'):
            step_func = 'when'
            step_text = line[5:].strip()
            result = search('At (.*) item enter "(.*)"',step_text)
            step_content = '    context.driver.find_element(by=MobileBy.ACCESSIBILITY_ID, value={}).send_keys("{}")\n\n'.format(result.group(1),result.group(2))
        elif line.startswith('Then'):
            step_func = 'then'
            step_text = line[5:].strip()
            result = search('Show a message "(.*)"',step_text)
            if 'Show a message' in line:
                step_content = '    display_element = context.driver.find_element(by=MobileBy.XPATH, value={}//android.view.ViewGroup[@content-desc="test-Error message"]/android.widget.TextView{});\n'.format("'", "'")
                step_content += '    value = display_element.text\n'
                step_content += '    assert value == "{}", "Not match"\n\n'.format(result.group(1))
            if 'Go to' in line:
                step_content = '    display_element = context.driver.find_element(by=MobileBy.XPATH, value={}//android.view.ViewGroup[@content-desc="test-Cart drop zone"]/android.view.ViewGroup/android.widget.TextView{});\n'.format("'", "'")
                step_content += '    value = display_element.text\n'
                step_content += '    assert value == "{}", "Not match"\n\n'.format(result.group(1))
        else:
            if step_func == 'when':
                step_text = line[3:].strip()
                steps.append(f"@{step_func}('{step_text}')\ndef step_impl(context):\n")
                if 'Tap' in line:
                    result = search('And Tap button (.*)',line.strip())
                    step_content = '    context.driver.find_element(by=MobileBy.ACCESSIBILITY_ID, value={}).click()\n\n'.format(result.group(1))
                    steps.append(step_content)
                else:
                    result = search('And At (.*) item enter "(.*)"',line.strip())
                    step_content = '    context.driver.find_element(by=MobileBy.ACCESSIBILITY_ID, value={}).send_keys("{}")\n\n'.format(result.group(1),result.group(2))
                    steps.append(step_content)
            continue
            
        steps.append(f"@{step_func}('{step_text}')\ndef step_impl(context):\n")

        if step_content == '':
            steps.append('    pass\n\n')
        else:
            steps.append(step_content)
    feature_file_path = feature_file_path.split('/')
    step_file_path = 'steps_backup'+'/'+feature_file_path[1]+'/step_'+feature_file_path[2].split('.')[0]+'.py'
    path = step_file_path.split('/')
    if not os.path.exists(path[0]+'/'+path[1]):
            os.makedirs(path[0]+'/'+path[1])
    with open(step_file_path, 'w') as f:
        f.write('from behave import given, when, then, step\n')
        f.write('from appium import webdriver\n')
        f.write('from appium.webdriver.common.mobileby import MobileBy\n')
        f.write('from selenium.webdriver.support.ui import WebDriverWait\n')
        f.write('from selenium.webdriver.support import expected_conditions as EC\n\n')
        f.writelines(steps)
        f.flush()
        f.close()
    return 'Script created successfully'
