from behave import given, when, then, step
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

@given('Open demo app and Access the LOGIN screen')
def step_impl(context):
    desired_caps = {
        "platformName": "Android",
        "appium:platformVersion": "12",
        "appium:deviceName": "Redmi Note 10 Pro",
        "appium:appPackage": "com.swaglabsmobileapp",
        "appium:appActivity": "com.swaglabsmobileapp.MainActivity"
    }

    context.driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_caps)

@when('At "test-Username" item enter "standard_user"')
def step_impl(context):
    context.driver.find_element(by=MobileBy.ACCESSIBILITY_ID, value="test-Username").send_keys("standard_user")

@when('At "test-Password" item enter "secret_sauce"')
def step_impl(context):
    context.driver.find_element(by=MobileBy.ACCESSIBILITY_ID, value="test-Password").send_keys("secret_sauce")

@when('Tap button "test-LOGIN"')
def step_impl(context):
    context.driver.find_element(by=MobileBy.ACCESSIBILITY_ID, value="test-LOGIN").click()

@then('Go to "PRODUCT" screen')
def step_impl(context):
    pass

