from behave import given, when, then, step
from appium import webdriver

@given('Open the demo app')
def step_impl(context):
    desired_caps = {
        "platformName": "Android",
        "platformVersion": "9.0",
        "deviceName": "device",
        "appPackage": "com.example.demo"
    }

    context.driver = webdriver.Remote("http://localhost:4723/wd/hub", desired_caps)

@when('Access the Login screen')
def step_impl(context):
    context.driver.current_activity = "com.example.demo.Login"

@then('Check Email item display value is "Email"')
def step_impl(context):
    display_element = context.driver.find_element_by_id("Email")
    value = display_element.getText()
    Assert.assertEquals(value, "Email")

context.driver.quit()