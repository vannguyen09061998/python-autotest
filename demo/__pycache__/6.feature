Feature: LOGIN
Scenario: Login successfully
Given Open demo app and Access the LOGIN screen
When At "test-Username" item enter "standard_user"
And At "test-Password" item enter "secret_sauce"
And Tap button "test-LOGIN"
Then Go to "PRODUCT" screen
